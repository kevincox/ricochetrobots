export const ROBOT_NAMES = [
	"Black",
	"Red",
	"Green",
	"Yellow",
	"Blue",
];

export const DIRECTION_NAMES = [
	"🡅",
	"🡄",
	"🡆",
	"🡇",
];
