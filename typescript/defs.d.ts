declare interface String {
	// AssetGraph magic for making a URL reference.
	toString(u: "url"): string;
}
