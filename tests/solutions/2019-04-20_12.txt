board 16 16
B N N B N N N N N N N N N B N N
W C C C C C C C C C C C C C C C
W C C C C C B C C C C C W C C C
W C C C C N C C C C C B C C C C
W C C C C C C C C C C C C C C C
W W C C C C C C C N W C C C C N
W N C C C C C C C C C C C W C C
B C C C N W C B B W C C C N C C
W C C C C C C B B W B C C C C C
W C C C N W C N N C C C C C C N
B C C C C C C C C C C C C C B C
W C C C C C C C C C C C C C C C
W C C C C C C W C C C C N W C C
W C C C C C B C C C W C C N C C
W C C W C C C C C N C C C C C C
W C C N C C W C C C C W C C C C
mirrors 8
2 1 / 4
7 4 \ 1
14 4 / 4
12 6 / 3
1 11 \ 2
7 10 / 3
12 9 \ 2
13 14 / 1
robots 5
0 6
11 3
5 2
9 13
4 7
target 0 13 6 4
solution 12
move 4 4 8
move 4 6 8
move 4 6 2
move 0 12 0
move 2 5 0
move 2 11 0
move 4 11 2
move 4 11 1
move 4 15 1
move 4 15 0
move 4 13 0
move 4 13 6
