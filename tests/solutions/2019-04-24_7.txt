board 16 16
B N N N N N B N N N B N N N N N
W C C C C C C C C C C C B C C C
W C C C C C C C C C C C C C C N
W B C C C C C C C W C C C C C C
W C C C C C C W C N C C C C C W
W C C C C C N C C C N W C C N C
B C N W C C C C C C C C C C C C
W C C N C C C B B W C C C C C C
W C C C C C C B B W C C C C C C
W C W C C C C N N C C C C C W C
W C N W C C C C C C W C C C N C
W C C C C C C B C N C C B C C C
W C C C C C C C C C C C C C C N
B C C C C C C C B C C C C C C C
W C C C C C W C C C N W C C C C
W C C C C N C W C C C C C W C C
mirrors 4
4 1 / 2
5 7 \ 3
4 8 / 1
1 13 \ 4
robots 5
8 13
5 14
14 4
2 9
0 13
target 0 9 10 2
solution 7
move 3 13 9
move 3 13 0
move 3 10 0
move 2 7 4
move 3 10 4
move 2 9 4
move 2 9 10
